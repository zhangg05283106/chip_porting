# LTS3.0与3.1分支编译差异

#### 介绍
    本文主要对比OpenHarmony-3.0-LTS与OpenHarmony-3.1-Release（3.1版与master编译方式对齐）两个分支在编译方面的差异，下载完代码后可参考如下三种方式进行编译;
|命令|3.0-LTS|3.1-Release |说明|
|--|--|--|--|
| Python安装的公共组件 命令： hb xxxx | 版本需求：<br> ohos-build <= 0.4.3 <br>支持子命令：<br> {bulid, set,env,clean,deps}<br>|版本需求:<br> ohos-build >= 0.4.6<br> 支持子命令:<br> {build,set,env, deps}<br>|组件安装: <br> Pip3 intall ohos-build ==x.x.x <br>因3.1引入了新的编译架构，ohos-build的这两个版本互不兼容，不能交叉使用 查询可用的ohos-build版本:<br> Pip3 index version ohos-build|
| bulid仓内部hb组件<br> 命令: <br>python build/lite/hb/main.py | 支持的子命令: <br>{build,set,env,clean,deps}<br>|支持子命令: <br>{build,set,env, deps}|内部hb组件路径统一在：build/lite/hb目录中。<br>3.1分支为了支持新的编译框架把编译命令放在<br>build/lite/hb_internal目录，但是调用方式仍然使用build/lite/hb/main.py|
|./ build.sh 脚本 命令: ./ build.sh --product-name {product-name}{options}|支持的options选项：<br>--ccache use ccache , defult: false<br>--jobs N run N jobs in parallel<br>--build-target build target name<br>--gn-args gn args<br>--export-para export env<br>--help ,-h print help info||通过脚步自动调用内部hb组件实现编译，需要先执行./build/prebuilt_download.sh脚本检查编译环境及相关工具<br>通过如下命令到vendor或者productdefine目录可以查询支持哪些{product-name}:<br>grep ''\ ''product_name'':'' -nr ./ vendor grep ''\ ''product_name'':'' -nr ./ productdefine|